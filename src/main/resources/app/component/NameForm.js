import React from 'react';

/**
 * 注意() 的使用， 容易犯错
 */
class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            fruit: 'coconut'
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        alert("Name -> " + this.state);
        event.preventDefault();
    };

    handleInputChange(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({[name]: value});
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="name">
                    Name:
                    <input id="name" name="name" type="text" value={this.state.name} onChange={this.handleInputChange}/>
                </label>
                <br/>
                <label htmlFor="micropost">
                    Micropost:
                    <textarea id="micropost" name="micropost" value={this.state.name} />
                </label>
                <br/>
                <label htmlFor="fruit">
                    <select name="fruit" id="fruit" value={this.state.fruit} onChange={this.handleInputChange}>
                        <option value="grapefruit">GrapeFruit</option>
                        <option value="lime">Lime</option>
                        <option value="coconut">Coconut</option>
                        <option value="Mango">Mango</option>
                    </select>
                    <p>{this.state.fruit}</p>
                </label>
                <br/>
                <input type="submit" value="Submit"/>
            </form>
        )
    }
}

export default NameForm;
