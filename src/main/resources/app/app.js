import React from 'react';
import ReactDOM from 'react-dom';
import Hello from './component/Hello';
import LikeButton from './component/LikeButton';
import Clock from './component/Clock';
import NameForm from './component/NameForm';


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employees:[]
        };
    }

    componentDidMount() {
        // do something here
        const main = this;
    }

    render() {
        return (
            <div>
                <Hello/>
                <LikeButton/>
                <Clock/>
                <NameForm/>
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react')
);
