package team.pi.apple.hooks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import team.pi.apple.entity.Employee;
import team.pi.apple.repository.EmployeeRepository;

/**
 * Created on 2017/8/27 上午10:02
 *
 * It implements Spring Boot’s CommandLineRunner
 * so that it gets run after all the beans
 * are created and registered.
 *
 * @author shuai.zhang
 */
@Component
public class DatabaseLoader implements CommandLineRunner {

    private final EmployeeRepository repository;

    @Autowired
    public DatabaseLoader(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(
            new Employee("Zhang", "San", "Hi")
        );
    }
}
