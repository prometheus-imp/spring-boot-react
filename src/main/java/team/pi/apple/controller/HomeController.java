package team.pi.apple.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created on 2017/8/25 下午5:28
 *
 * @author shuai.zhang
 */
@Controller
public class HomeController {


    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
