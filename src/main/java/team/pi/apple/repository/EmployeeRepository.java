package team.pi.apple.repository;

import org.springframework.data.repository.CrudRepository;
import team.pi.apple.entity.Employee;

/**
 * Created on 2017/8/27 上午9:59
 *
 * @author shuai.zhang
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
