package team.pi.apple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created on 2017/8/25 下午5:25
 *
 * @author shuai.zhang
 */
@SpringBootApplication
public class ReactApp {

    public static void main(String[] args) {
        SpringApplication.run(ReactApp.class, args);
    }
}
